package irc

import "fmt"
import "strings"
import "os"
import "io/ioutil"

func GetServerVersion() string{
    file, err := os.Open(".git/HEAD")
    if err != nil {
        panic(err)
    }
    defer file.Close()
    b, err := ioutil.ReadAll(file)
    if strings.HasPrefix(string(b), "ref:") {
        v_file, err := os.Open(".git/" + strings.Split(string(b), " ")[1])
        if err != nil {
            panic(err)
        }
        defer v_file.Close()
        ver, err := ioutil.ReadAll(v_file)
        return string(ver)
    }
    return ""
}

func SendReply(c Client, code [2]string, args ...interface{}) {
    result := fmt.Sprintf(
        ":%s %s %s %s\n",
        c.Server.Name,
        code[0],
        c.Nick,
        fmt.Sprintf(code[1], args...),
    )
    fmt.Printf(result)
    c.Co.Write([]byte(string(result)))
}
