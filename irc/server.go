package irc

import "errors"

type Server struct {
    Name    string
    Clients map[string]Client
    Chans   map[string]Channel
}

func NewServer(n string, c map[string]Client, ch map[string]Channel) Server {
    return Server{n, c, ch}
}

func (s Server) FindByNick(n string) (Client, error){
    for _, c := range s.Clients {
        if c.Nick == n {
            return c, nil
        }
    }
    return Client{}, errors.New("Client not on this server")
}
