package irc

import "fmt"

type Channel struct {
    Name        string
    Topic       string
    Type        string
    Modes       map[string]string
    Clients     map[string]Client
}

func NewChannel(n string, t string, ty string, m map[string]string, c map[string]Client) Channel {
    return Channel{n, t, ty, m, c}
}


func (c Channel) JOIN(cl Client, k string){
    _, exists := c.Clients[cl.Nick]
    if exists {
        return
    }
    key, _ := c.Modes["+k"]
    if k != key {
        SendReply(cl, ERR_BADCHANNELKEY, c.Name)
        return
    }
    c.Clients[cl.Nick] = cl
    cl.Chans[c.Name] = c
    message := fmt.Sprintf(":%s!%s@%s JOIN %s\n", cl.Nick, cl.User, cl.Host, c.Name)
    for _, cli := range c.Clients {
       fmt.Printf(message)
       cli.Co.Write([]byte(message))
    }
    c.TOPIC(cl, false)
    for _, cli := range c.Clients {
        SendReply(cl, RPL_NAMREPLY, c.Type, c.Name, "", cli.Nick)
    }
    SendReply(cl, RPL_ENDOFNAMES, c.Name)
}
func (c Channel) PART(cl Client, m string){
    msg := fmt.Sprintf(":%s!%s@%s PART %s :%s\n", cl.Nick, cl.User, cl.Host, c.Name, m)
    for _, cli := range c.Clients {
        fmt.Printf(msg)
        cli.Co.Write([]byte(msg))
    }
    delete(c.Clients, cl.Nick)
    delete(cl.Chans, c.Name)
    if len(c.Clients) == 0 {
        delete(cl.Server.Chans, c.Name)
    }
}

func (c Channel) SETTOPIC(cl Client, t string){
    c.Topic = t
    for _, cl := range c.Clients {
        cl.Co.Write([]byte(fmt.Sprintf(":%s!%s@%s TOPIC %s :%s\n", cl.Nick, cl.User, cl.Host, c.Name, t)))
    }
    cl.Server.Chans[c.Name] = c
}
func (c Channel) TOPIC(cl Client, force bool){
    if c.Topic != "" {
        SendReply(cl, RPL_TOPIC, c.Name, c.Topic)
    } else if force {
        SendReply(cl, RPL_NOTOPIC, c.Name)
    }
}

func (c Channel) PRIVMSG(cl Client, m string) {
    msg := fmt.Sprintf(":%s!%s@%s PRIVMSG %s %s\n", cl.Nick, cl.User, cl.Host, c.Name, m)
    for _, cli := range c.Clients {
        if cli.Nick == cl.Nick {
            continue
        }
        cli.Co.Write([]byte(msg))
    }
}

