package irc

import "net"
import "fmt"

type Client struct {
    Nick        string
    User        string
    Realname    string
    Host        string
    Co          net.Conn
    Server      Server
    Chans       map[string]Channel
}

func NewClient(n string, u string, r string, h string, c net.Conn, s Server, ch map[string]Channel) Client {
    return Client{n, u, r, h, c, s, ch}
}

func (c Client) SetNick(n string) {
    old_nick := c.Nick
    c.Nick = n
    c.Server.Clients[c.Co.RemoteAddr().String()] = c
    notified := make(map[string]bool)
    for _, ch := range c.Chans {
        delete(ch.Clients, old_nick)
        ch.Clients[n] = c
        for _, cl := range ch.Clients {
            _, exists := notified[cl.Nick]
            if !exists {
                cl.Co.Write([]byte(fmt.Sprintf(":%s NICK %s\n", old_nick, n)))
                notified[cl.Nick] = true
            }
        }
   }
}
