package main

import "net"
import "fmt"
import "bufio"
import "io"
import "strings"

import "go_server/irc"


var SERVER = irc.NewServer(
    "go_server",
    map[string]irc.Client{},
    map[string]irc.Channel{},
)

func handleErr(err error) {
    if err != nil {
        panic(err)
    }
}

func handleMessage(m string, c net.Conn) {
    fmt.Println(m)
    var addr    string = c.RemoteAddr().String()

    message, err := irc.ParseMessage(m)
    handleErr(err)

    switch message.Command {
        case "NICK":
            new_nick := message.Parameters[0]
            _, exists := SERVER.Clients[addr]
            if exists {
                SERVER.Clients[addr].SetNick(new_nick)
            } else {
                SERVER.Clients[addr] = irc.NewClient(
                    new_nick,
                    "",
                    "",
                    "",
                    c,
                    SERVER,
                    make(map[string]irc.Channel),
                )
            }
        case "USER":
            client, exists := SERVER.Clients[addr]
            if !exists {
                client = irc.NewClient(
                    message.Parameters[0],
                    "",
                    "",
                    "",
                    c,
                    SERVER,
                    make(map[string]irc.Channel),
                )
            }
            client.User = message.Parameters[0]
            client.Realname = message.Parameters[3]
            client.Host = strings.Split(addr, ":")[0]
            SERVER.Clients[addr] = client
            irc.SendReply(client, irc.RPL_WELCOME, client.Nick, client.User, client.Host)
        case "JOIN":
            if len(message.Parameters) == 1{
                if message.Parameters[0] == "0" {
                    for _, c := range SERVER.Clients[addr].Chans {
                        c.PART(SERVER.Clients[addr], "")
                    }
                } else {
                    cToJoin := strings.Split(message.Parameters[0], ",")
                    for _, c := range cToJoin {
                        cha, exists := SERVER.Chans[c]
                        if exists {
                            cha.JOIN(SERVER.Clients[addr], "")
                        } else {
                            cha = irc.NewChannel(
                                c,
                                "",
                                "=",
                                make(map[string]string),
                                make(map[string]irc.Client),
                            )
                            SERVER.Chans[c] = cha
                            cha.JOIN(SERVER.Clients[addr], "")
                        }
                    }
                }
            } else if len(message.Parameters) == 2 {
                chans := strings.Split(message.Parameters[0], ",")
                keys := strings.Split(message.Parameters[1], ",")
                if len(keys) <= len(chans) {
                    for i, k := range keys {
                        cha, exists := SERVER.Chans[chans[i]]
                        if exists {
                            cha.JOIN(SERVER.Clients[addr], k)
                        } else {
                            cha = irc.NewChannel(
                                chans[i],
                                "",
                                "=",
                                map[string]string{"+k":k},
                                make(map[string]irc.Client),
                            )
                            SERVER.Chans[chans[i]] = cha
                            cha.JOIN(SERVER.Clients[addr], k)
                        }
                    }
                } else {
                    irc.SendReply(SERVER.Clients[addr], irc.ERR_NEEDMOREPARAMS, "JOIN")
                }
            }
        case "MODE":
            ch, exists := SERVER.Chans[message.Parameters[0]]
            if exists {
                reply_modes := ""
                for m, val := range ch.Modes {
                    reply_modes = reply_modes  + m + " " + val + " "
                }
                irc.SendReply(SERVER.Clients[addr], irc.RPL_CHANNELMODEIS, ch.Name, reply_modes)
            } else {
                irc.SendReply(SERVER.Clients[addr], irc.ERR_NOSUCHCHANNEL, message.Parameters[0])
            }
        case "PART":
            msg := ""
            if len(message.Parameters) == 2 {
                msg = message.Parameters[1]
            }
            chans := strings.Split(message.Parameters[0], ",")
            for _, ch := range chans {
                cha, exists := SERVER.Chans[ch]
                if !exists {
                    irc.SendReply(SERVER.Clients[addr], irc.ERR_NOSUCHCHANNEL, ch)
                    continue
                } else {
                    _, exists := cha.Clients[SERVER.Clients[addr].Nick]
                    if !exists {
                        irc.SendReply(SERVER.Clients[addr], irc.ERR_NOTONCHANNEL, ch)
                    } else {
                        cha.PART(SERVER.Clients[addr], msg)
                    }
                }
            }


        case "LIST":
            if len(message.Parameters) == 0 {
                //irc.SendReply(SERVER.Clients[addr], irc.RPL_LISTSTART) obsolete since RFC2812
                for _, ch := range SERVER.Chans {
                    irc.SendReply(SERVER.Clients[addr], irc.RPL_LIST, ch.Name, len(ch.Clients), ch.Topic)
                }
            }
            if len(message.Parameters) == 1 {
                for _, cha := range strings.Split(message.Parameters[0], ",") {
                    ch, exists := SERVER.Chans[cha]
                    if exists {
                        irc.SendReply(SERVER.Clients[addr], irc.RPL_LIST, ch.Name, len(ch.Clients), ch.Topic)
                    }
                }
            }
            irc.SendReply(SERVER.Clients[addr], irc.RPL_LISTEND)
        case "PRIVMSG":
            client := SERVER.Clients[addr]
            var destC irc.Client
            destC, err := SERVER.FindByNick(message.Parameters[0])
            if err == nil {
                msg := fmt.Sprintf(":%s!%s@%s PRIVMSG %s %s\n", client.Nick, client.User, client.Host, destC.Nick, strings.Join(message.Parameters[1:], " "))
                fmt.Printf(msg)
                destC.Co.Write([]byte(msg))
            }
            destChan, exists := SERVER.Chans[message.Parameters[0]]
            if exists {
                destChan.PRIVMSG(client, strings.Join(message.Parameters[1:], " "))
            } else {
            }
        case "TOPIC":
            switch len(message.Parameters) {
                case 1:
                    for _, c := range strings.Split(message.Parameters[0], ",") {
                        SERVER.Chans[c].TOPIC(SERVER.Clients[addr], true)
                    }
                case 2:
                    for _, c := range strings.Split(message.Parameters[0], ",") {
                        SERVER.Chans[c].SETTOPIC(SERVER.Clients[addr], strings.Split(message.Parameters[1],":")[1])
                    }
            }
        case "PING":
            c.Write([]byte(fmt.Sprintf("PONG %s\n", SERVER.Name)))
        case "ISON":
            list_nicks := ""
            for _, nick := range message.Parameters {
                if strings.HasPrefix(nick, ":") {
                    nick = strings.Split(nick, ":")[1]
                }
                _, err := SERVER.FindByNick(nick)
                if err == nil {
                    list_nicks = list_nicks + nick + " "
                }
            }
            irc.SendReply(SERVER.Clients[addr], irc.RPL_ISON, list_nicks)
        default:
            irc.SendReply(SERVER.Clients[addr], irc.ERR_UNKNOWNCOMMAND, message.Command)
    }
}


func handleConnection(c net.Conn) {
    fmt.Printf("Connection from %s\n", c.RemoteAddr().String())
    for {
        netData, err := bufio.NewReader(c).ReadString('\n')
        if err == io.EOF {
            fmt.Printf("Connection lost from %s\n", c.RemoteAddr().String())
            break
        }
        handleErr(err)
        go handleMessage(string(netData), c)
    }
    c.Close()
}


func main() {


    ADDRESS := "0.0.0.0"
    PORT := ":" + "6667"
    listener, err := net.Listen("tcp", ADDRESS + PORT)
    handleErr(err)
    defer listener.Close()

    for {
        connection, err := listener.Accept()
        handleErr(err)
        go handleConnection(connection)
    }
}
