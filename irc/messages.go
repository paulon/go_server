package irc

import "strings"

var (
    RPL_WELCOME =       [...]string{"001", ":Welcome to the Internet Relay Network %s!%s@%s"}
    RPL_YOURHOST =      [...]string{"002", ":Your host is %s, running version %s"}
    RPL_CREATED =       [...]string{"003", ":This server was created %s"}

    RPL_TRYAGAIN =      [...]string{"263", "%s :%s"}

    RPL_ISON =          [...]string{"303", ":%s"}
    RPL_LISTSTART =     [...]string{"321", "Channel :User Name"} //Obsolete since RFC2812
    RPL_LIST =          [...]string{"322", "%s %d :%s"}
    RPL_LISTEND =       [...]string{"323", ":End of LIST"}
    RPL_CHANNELMODEIS = [...]string{"324", "%s %s"}
    RPL_NOTOPIC =       [...]string{"331", "%s :No topic is set"}
    RPL_TOPIC =         [...]string{"332", "%s :%s"}
    RPL_NAMREPLY =      [...]string{"353", "%s %s :%s%s"}
    RPL_ENDOFNAMES =    [...]string{"366", "%s :End of NAMES list"}

    ERR_NOSUCHCHANNEL = [...]string{"403", "%s :No such channel"}
    ERR_UNKNOWNCOMMAND =[...]string{"421", "%s :Unknown command"}
    ERR_NOTONCHANNEL =  [...]string{"442", "%s :You're not on that channel"}
    ERR_NEEDMOREPARAMS =[...]string{"461", "%s :Not enough parameters"}
    ERR_BADCHANNELKEY = [...]string{"475", "%s :Cannot join channel (+k)"}
)

type Message struct {
    Tags        map[string]string
    Source      string
    Command     string
    Parameters  []string
}

func NewMessage(t map[string]string, s string, c string, p []string) Message {
    return Message{t, s, c, p}
}

func ParseTags(s string) map[string]string {
    tags := make(map[string]string)

    return tags
}

func ParseParameters(s string) []string{
    return strings.Fields(s)
}

func ParseMessage(s string) (Message, error){
    var tags map[string]string
    var source string
    var command string
    var param []string
    s_list := strings.Fields(s)
    if strings.HasPrefix(s_list[0],"@"){
        tags = ParseTags(s_list[0])
        s_list = s_list[1:]
    }
    if strings.HasPrefix(s_list[0], ":") {
        s_list = s_list[1:]
    }
    command = s_list[0]
    param = ParseParameters(strings.Join(s_list[1:], " "))
    return NewMessage(tags, source, command, param), nil
}
